# Blogs

[Epic React](https://epicreact.dev/one-react-mistake-thats-slowing-you-down/)

# Frameworks / Libraries / Etc

([Svelte](https://svelte.dev/)
+
[Sapper](https://sapper.svelte.dev/)) --> [SvelteKit](https://kit.svelte.dev/)

[After.js](https://github.com/jaredpalmer/after.js)
+
[Razzle](https://github.com/jaredpalmer/razzle)

[Next.js](https://nextjs.org/)

[HTML5 Boilerplate](https://github.com/h5bp/html5-boilerplate)

[Grafana](https://grafana.com/)

[AnimXYZ](https://animxyz.com/)

[Testing Library](https://testing-library.com/)

[NodeGui](https://github.com/nodegui/nodegui)

[Chakra UI](https://chakra-ui.com/)

[Astro](https://astro.build/)

[FastAPI](https://fastapi.tiangolo.com/)

[Remix](https://remix.run/)

# Databases etc

[Workbox](https://developers.google.com/web/tools/workbox)

[Knex.js](http://knexjs.org/)

[RethinkDB](https://rethinkdb.com/)

[Datomic](https://www.datomic.com/)

[React Query](https://react-query.tanstack.com/)

[Relay](https://relay.dev/)

[Supabase](https://supabase.io/)

[Slonik](https://github.com/gajus/slonik)

[Schemats](https://github.com/sweetiq/schemats)

# Components

[Draft.js](https://draftjs.org/)

# Testing (And Related)

[Storybook](https://storybook.js.org/)

[React Styleguidist](https://react-styleguidist.js.org/)

[Coverage](https://coverage.readthedocs.io/en/coverage-5.5/)

# Logging

[LogRocket](https://logrocket.com/)

[Sentry](https://sentry.io/welcome/)

# Automation

[Zapier](https://zapier.com/)

# Cloud

[Netlify](https://www.netlify.com/)

[Hasura](https://hasura.io/)

[Aiven](https://aiven.io/)

[Cloudflare](https://www.cloudflare.com/)

[Krane](https://github.com/Shopify/krane)

[Supabase](https://supabase.io/)

[Fly.io](https://fly.io/)

[Render](https://render.com/)

[Knative](https://knative.dev/)

[DevSpave](https://devspace.sh/)

# Util Tool Helpers

[Underscore.js](https://underscorejs.org/)

[Lodash](https://lodash.com/)
+
[Lodash/fp](https://github.com/lodash/lodash/wiki/FP-Guide)

[fp-ts](https://github.com/gcanti/fp-ts)

[Ramda](https://ramdajs.com/)

[Remeda](https://remedajs.com/)

[Runtypes](https://github.com/pelotom/runtypes)

[Google ZX](https://github.com/google/zx)

# CMS

[Sanity](https://www.sanity.io/)

[Prismic](https://prismic.io/)

# Reading

[Clean Code Javascript](https://github.com/ryanmcdermott/clean-code-javascript)

# Design

[Figma](https://www.figma.com/)

[System UIcons](https://systemuicons.com/)

[Dribbble](https://dribbble.com/)

[HTML5 UP](https://html5up.net/)

[Excalidraw](https://excalidraw.com/)

[Gradient Magic](https://www.gradientmagic.com/)

# Other

[Web Skills Collection](https://www.outpan.com/app/0aa0ddba83/web-skills)

# ML

[PyTorch](https://pytorch.org/)

[TensorFlow](https://www.tensorflow.org/)

[scikit-learn](https://scikit-learn.org/stable/)

[Jupyter](https://jupyter.org/)
