# Part 1

## Suggesting line break opportunities

https://developer.mozilla.org/en-US/docs/Web/CSS/hyphens#:~:text=There%20are%20two%20Unicode%20characters,the%20hyphen%20is%20still%20rendered.

E.g. `Jääkiekko&shy;maalivahti`

## Mapped type with required

```
interface X {
    value1: string;
    value2?: string;
}
```

```
type XRequired = {
    [T in keyof X]-?: string;
};
```

## Union types in a list
```
const myTypes = ['Car', 'Cat', 'Coffee', 'Coat''] as const;

interface SomeInterface {
    type: typeof myTypes[number];
}
```

## Preload + low/highres

```
<link rel="preload" href="img_lowres.jpg" as="image" />
```

```
background-image: url('img_highres.jpg'), url('img_lowres.jpg');

/* You also need to duplicate other background-xyz properties, like: */
background-size: cover, cover;
```

## Lazyload images

```
<img loading=lazy>
```